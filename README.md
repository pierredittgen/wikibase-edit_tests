# wikibase-edit tests

## Install

```bash
npm install
```

## Config

Configuration settings are defined through environment variables defined in `.env` file.

first, create `.env` file from `.env.example` file and adapt to your need.

Do not commit `.env` file

## Run

Using [dotenv](https://www.npmjs.com/package/dotenv):

### Change french label

A script to change french label of an existing item

```bash
ts-node -r dotenv/config set_french_label.ts <itemId> <frenchLabel>
```

### Add spot prices

A script to add spot prices to an existing item

```bash
ts-node -r dotenv/config add_spot_prices.ts [ --item 1 ] <itemId> <nbOfPricestoAdd>
```

Note : using `--item 1` uses [wbeditentity](https://www.wikidata.org/w/api.php?action=help&modules=wbeditentity) API instead of [wbsetclaim](https://www.wikidata.org/w/api.php?action=help&modules=wbsetclaim) (default)

### Remove spot prices

A script to remove spot prices of an existing item

```bash
ts-node -r dotenv/config remove_spot_prices.ts <itemId>
```

Note: only official spot prices are targeted here
