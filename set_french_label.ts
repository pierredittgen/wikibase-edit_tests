// Set item french label command line script

// Handle command line arguments
const cliArgs = process.argv.slice(1);
if (cliArgs.length !== 3) {
  throw `usage: ${cliArgs[0]} <wikibaseItem> <label>`;
}
const wikibaseItemId = cliArgs[1];
const label = cliArgs[2];

// Wikibase config from environment variables
const generalConfig = {
  instance: process.env.WIKIBASE_INSTANCE,
  credentials: {
    username: process.env.WIKIBASE_USER,
    password: process.env.WIKIBASE_PASSWORD,
  },
};

const wbEdit = require("wikibase-edit")(generalConfig);

console.log(`Set french label for item ${wikibaseItemId}: ${label}...`);
wbEdit.label
  .set({ id: wikibaseItemId, language: "fr", value: label })
  .then(() => {
    console.log("done.");
  });
