// Add [n] prices to an item

// Entity ids are configured with environment variables
const officialSpotPriceProperty = process.env.OFFICIALSPOTPRICEPROPERTY;
const tradingDayProperty = process.env.TRADINGDAYPROPERTY;
const dfihStockNotationIdProperty = process.env.DFIHSTOCKNOTATIONIDPROPERTY;
const dfihStockSectorProperty = process.env.DFIHSTOCKSECTORPROPERTY;
const dfihSampleStockSectorItemId = process.env.DFIHSAMPLESTOCKSECTORITEMID;
const sourceDatabaseProperty = process.env.SOURCEDATABASEPROPERTY;
const sourceDatabaseItemId = process.env.SOURCEDATABASEITEMID;
const retrievedProperty = process.env.RETRIEVEDPROPERTY;

const fmtDate = (d: Date) => d.toISOString().substr(0, 10);

function computePriceClaimObject(
  itemId: string,
  spotPrice: number,
  tradingDay: Date,
  stockNotationId: string,
  retrievedDate: Date,
  withIdAndProperty: boolean = true
): object {
  const id = itemId;
  const property = officialSpotPriceProperty;
  const value = spotPrice;
  const qualifiers = {
    [tradingDayProperty as string]: fmtDate(tradingDay),
    [dfihStockNotationIdProperty as string]: stockNotationId,
    [dfihStockSectorProperty as string]: dfihSampleStockSectorItemId,
  };
  const references = [
    {
      [sourceDatabaseProperty as string]: sourceDatabaseItemId,
      [retrievedProperty as string]: fmtDate(retrievedDate),
    },
  ];

  return withIdAndProperty
    ? {
        id,
        property,
        value,
        qualifiers,
        references,
      }
    : { value, qualifiers, references };
}

function* iteratorPriceClaimObjects(
  wikibaseItem: string,
  claimCount: number,
  startTradingDay: Date,
  withIdAndProperty: boolean = true
) {
  let tradingDay = startTradingDay;
  let retrievedDate = new Date();

  for (let no = 1; no <= claimCount; no++) {
    // Add a week every 10 prices
    if (no % 10 === 0) {
      tradingDay.setDate(tradingDay.getDate() + 7);
    }

    // Create claim object from params
    yield computePriceClaimObject(
      wikibaseItem,
      Number((Math.random() * 80 + 10).toFixed(2)),
      tradingDay, // YYYY-MM-DD
      "3", // sample notation id
      retrievedDate,
      withIdAndProperty
    );
  }
}

// Handle command line arguments
const { ArgumentParser } = require("argparse");
const parser = new ArgumentParser({
  description: "Add spot prices to a wikibase item",
});
parser.add_argument("wikibaseItem", {
  help: 'Wikibase item id (starting with "Q")',
});
parser.add_argument("priceCount", { help: "Nb of prices to add to the item" });
parser.add_argument("-i", "--item", {
  type: "int",
  default: 0,
  help: "Update item at once, else sequentially create claims (default)",
});
const args = parser.parse_args();

// Wikibase config from environment variables
const generalConfig = {
  instance: process.env.WIKIBASE_INSTANCE,
  credentials: {
    username: process.env.WIKIBASE_USER,
    password: process.env.WIKIBASE_PASSWORD,
  },
};
const wbEdit = require("wikibase-edit")(generalConfig);

const claims = [
  ...iteratorPriceClaimObjects(
    args.wikibaseItem,
    args.priceCount,
    new Date("1870-01-01"),
    args.item === 0
  ),
];

// Setting claims on item
if (args.item) {
  console.log(
    `Setting ${args.priceCount} price(s) to item ${args.wikibaseItem}`
  );
  wbEdit.entity
    .edit({
      id: args.wikibaseItem,
      claims: {
        [officialSpotPriceProperty as string]: claims,
      },
    })
    .then(() => {
      console.log("done.");
    })
    .catch((err: any) => console.log("An error occured: ", err));
  // Creating claims one by one
} else {
  console.log(`creating ${args.priceCount} claim(s)`);

  // Run them concurrently raise a 'Edit conflict' error
  //claims.map((c) => wbEdit.claim.create(c));

  // Run them sequentially
  claims.reduce((accPromise: Promise<void>, claim: object, idx: number) => {
    return accPromise.then(() => {
      console.log(`adding claim ${idx + 1}/${args.priceCount}`);
      return wbEdit.claim.create(claim);
    });
  }, Promise.resolve());
  console.log("done.");
}
