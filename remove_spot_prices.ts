// Add [n] prices to an item

// Entity ids are configured with environment variables
const officialSpotPriceProperty = process.env.OFFICIALSPOTPRICEPROPERTY;

// https://futurestud.io/tutorials/split-an-array-into-smaller-array-chunks-in-javascript-and-node-js
function chunk(items: any[], size: number) {
  const chunks = [];
  items = [...items];

  while (items.length) {
    chunks.push(items.splice(0, size));
  }

  return chunks;
}

function retrieveSpotPriceGuids(wbk: any, itemId: string): string[] {
  const url = wbk.getEntities({
    ids: [itemId],
    languages: ["en"],
    props: ["info", "claims"],
  });
  console.log({ url });

  // response
  // response.entities.Q199058.claims.P53[n].id

  return [];
}

function main() {
  // Handle command line arguments
  const { ArgumentParser } = require("argparse");
  const parser = new ArgumentParser({
    description: "Remove spot prices from a wikibase item",
  });
  parser.add_argument("wikibaseItem", {
    help: 'Wikibase item id (starting with "Q")',
  });
  const args = parser.parse_args();

  // Wikibase config from environment variables
  const generalConfig = {
    instance: process.env.WIKIBASE_INSTANCE,
    credentials: {
      username: process.env.WIKIBASE_USER,
      password: process.env.WIKIBASE_PASSWORD,
    },
  };

  const WBK = require("wikibase-sdk");
  const wbk = WBK({
    instance: process.env.WIKIBASE_INSTANCE,
    sparqlEndpoint: process.env.SPARQL_ENDPOINT_URL,
  });
  const wbEdit = require("wikibase-edit")(generalConfig);
  const fetch = require("node-fetch");

  const url = wbk.getEntities({
    ids: [args.wikibaseItem],
    languages: ["en"],
    props: ["claims"],
  });

  fetch(url)
    .then((response: any) => response.json())
    .then((data: any) => {
      const entities = data.entities || {};
      if (!entities[args.wikibaseItem]) {
        console.error(`Wikibase item ${args.wikibaseItem} not found.`);
        return;
      }
      const claims = entities[args.wikibaseItem].claims;
      let guids: string[] = [];
      if (officialSpotPriceProperty && claims[officialSpotPriceProperty]) {
        guids = claims[officialSpotPriceProperty].map((claim: any) => claim.id);
      }
      console.info(
        `${guids.length} official spot price claims to remove on this item`
      );
      if (guids.length == 0) {
        console.info("nothing to do.");
        return;
      }

      // Claim removal is limited to 500 claims per request
      chunk(guids, 500).reduce((accPromise: Promise<void>, guids: string[]) => {
        return accPromise.then(() => {
          console.log(`removing ${guids.length} claims(s)`);
          return wbEdit.claim.remove({ guid: guids });
        });
      }, Promise.resolve());
    })
    .catch((err: any) => console.log("An error occurred: ", err));
  return;
}

main();
